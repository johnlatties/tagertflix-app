import styled from 'styled-components/native'
import { theme } from '../../styles';
import { Dimensions } from 'react-native'
const { height, width } = Dimensions.get('window')
export const Container = styled.View`
  flex: 1;
  background-color: ${theme.primary};
  padding-top: ${theme.STATUSBAR_HEIGHT}px;
  justify-content: center;
  align-items: center;
`

export const Logo = styled.Image``

export const Form = styled.View`
  shadow-color: 'rgba(30, 29, 34, 0.25)';
  shadow-offset: { width: 0, height: 2};
  elevation: 15;
  background-color: ${theme.auxOne};
  border-radius: 10px;
  width: 80%;
  justify-content: center;
  align-items: center;
  margin-top: 30;
  margin-bottom: 30;
  height: ${height/2}
`

export const Input = styled.TextInput.attrs({
  placeholderTextColor: theme.auxThree
})`
  padding: 12px 15px;
  border-radius: 4px;
  font-size: 16px;
  color: ${theme.secundary};
  border-bottom-color: ${theme.auxThree};
  border-bottom-width: 1;
  text-align: left;
  width: 80%;
`

export const InputArea = styled.View`
  flex: 2;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
`

export const ActionArea = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  width: 100%;
`

export const Button = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  border-radius: 12;
  background-color: ${theme.auxTwo};
  width: 50%;
  shadow-color: rgba(30, 29, 34, 0.25);
  shadow-offset: { width: 0, height: 2};
  elevation: 15;
  padding-top: 15;
  padding-bottom: 15;
`

export const ButtonTitle = styled.Text`
  font-size: 25;
  color: ${theme.secundary};
`

export const MsgError = styled.Text`
  font-size: 14;
  color: ${theme.auxTwo}
`