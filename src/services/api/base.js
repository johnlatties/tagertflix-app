import axios from 'axios'
import { getUserToken } from '../data';

const getUrl = () => 'http://ec2-54-159-36-107.compute-1.amazonaws.com/api'

const setToken = async (config) => {
  const token =  await getUserToken()
  if (token) config.headers.Authorization = `TARGET ${token}`
  return config
}

const apiClient = axios.create({
  baseURL: getUrl(),
  headers: { 'Content-Type': 'application/json' }
})

apiClient.interceptors.request.use(
  async (config) => {
    const newConfig = await setToken(config)
    return newConfig
  },
  (error) => {
    return Promise.reject(error);
  }
)

export default apiClient