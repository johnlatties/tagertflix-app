import apiClient from './base'

export const likeTvShow = async ({ id }) => {
  try {
    const { data } = await apiClient.post('/user/like-show', { id })
    return data

  } catch (error) {
    console.log('Error', error)
  }
}

export const unlikeTvShow = async ({ id }) => {
  try {
    const { data } = await apiClient.post('/user/unlike-show', { id })
    return data
  } catch (error) {
    console.log('Error', error)
  }
}


export const getFavorites = async ({ id }) => {
  try {
    const { data } = await apiClient.get('/user/favorites')
    return data
  } catch (error) {
    console.log('Error', error)
  }
}
