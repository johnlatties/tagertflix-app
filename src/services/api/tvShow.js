import apiClient from './base'

export const getMovies = async () => {
  try {
    const { data } = await apiClient.get('/movies')
    return data
  } catch (error) {
    console.log('getMovies', error)
  }
}

export const getSeries = async () => {
  try {
    const { data } = await apiClient.get('/series')
    return data
  } catch (error) {
    console.log('getSeries', error)
  }
}
export const getHightlight = async () => {
  try {
    const { data } = await apiClient.get('/highlights')
    console.log('getHightlight', data)
    return data
  } catch (error) {
    console.log('getHightlight', error)
  }
}