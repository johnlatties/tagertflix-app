import apiClient from './base'

export const signin = async (data) => {
  try {
    const response = await apiClient.post('/signin', data)
    return {
      result: response.data
    }
  } catch (error) {
    console.log('ERRR', error)
    const { response } = error
    return {
      error: response.data
    }
  }
}