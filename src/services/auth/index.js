import { getUserToken, removeUserData } from "../data";

export const isAutheticate = async () => {
  const token = await getUserToken()
  return !!token
}

export const singOut = async (gotLogin) => {
  removeUserData()
  gotLogin()
}