import { AsyncStorage } from 'react-native'

const USER_KEY = '@user'

export const getUserToken = async () => {
  const { token } = await getUserData() || {}
  return token
}

export const getUserData = async () => {
  const data = await AsyncStorage.getItem(USER_KEY)
  return JSON.parse(data)
}

export const saveUserData = async (data) => {
  await AsyncStorage.setItem(USER_KEY, JSON.stringify(data))
}

export const removeUserData = async () => {
  await AsyncStorage.removeItem(USER_KEY)
}

