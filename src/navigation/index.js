import {
  createSwitchNavigator,
  createAppContainer,
  createBottomTabNavigator
} from 'react-navigation'

import Favorites from '../views/Favorites'
import Home from '../views/Home'
import Profile from '../views/Profile'
import Login from '../views/Login'
import Details from '../views/Details';
import { theme } from '../styles';



const appTabNavigator = createBottomTabNavigator({
  Favorites: {
    screen: Favorites
  },
  Home: {
    screen: Home
  },
  Profile: {
    screen: Profile
  }
},
  {
    initialRouteName: 'Home',
    tabBarOptions: {
      activeTintColor: theme.auxTwo,
      inactiveTintColor: theme.secundary,
      style: {
        backgroundColor: theme.auxOne,
        borderTopWidth: 0,
        shadowOffset: { width: 5, height: 3 },
        shadowColor: 'black',
        shadowOpacity: 0.5,
        elevation: 5
      }
    }
  })

const switchNavigator = createSwitchNavigator({
  Login: { screen: Login },
  Home: { screen: appTabNavigator },
  Details: Details
})

const Navigator = createAppContainer(switchNavigator)

export default Navigator