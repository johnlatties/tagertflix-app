
import { Platform, NativeModules } from 'react-native'
const getStatusBarHeight = () => {
  const { StatusBarManager } = NativeModules
  return Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT
}


export const theme = {
  primary: "#1c1c1c",
  secundary: "#FFFFFF",
  auxOne: "#2a2a2a",
  auxTwo: "#fd4d57",
  auxThree: "#555",
  STATUSBAR_HEIGHT: getStatusBarHeight()
}